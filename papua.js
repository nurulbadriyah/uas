import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/papua.jpg')}/>,teks:<Text>1. Tari Musyoh</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Masuk ke dalam 34 tari tradisional asal Indonesia, Tari Musyoh merupakan kepercayaan masyarakat Papua sebagai tarian ritual dalam upaya pengusiran arwah orang-orang yang meninggal karena kecelakaan.
                Masyarakat Papua percaya bahwa ketika ada seseorang yang meninggal karena kecelakaan, maka arwah yang bersangkutan tidak akan tenang.
                Nah, dikarenakan hal itulah Tarian Musyoh diperagakan sebagai jalan untuk menenangkan arwah tersebut.</Text> ,no:2},
                ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
               
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;