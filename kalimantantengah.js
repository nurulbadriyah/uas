import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/kalimantantengah.jpg')}/>,teks:<Text>1. Tari Giring-giring</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Tari Giring-Giring yang berasal dari Kalimantan Tengah juga tidak boleh ketinggalan untuk masuk ke dalam salah satu dari 34 tari tradisional asal Indonesia.

Menggunakan tongkat sebagai atributnya, tarian ini diperagakan sebagai bentuk ekspresi kebahagiaan dan rasa senang masyarakat setempat.

Nama Giring-Giring itu sendiri diambil dari nama tongkat yang dimainkan oleh para penari dan berasal dari Suku Dayak Maanyan yang pada saat itu mendiami daerah Kabupaten Barito Timur dan Kabupaten Barito Selatan Provinsi Kalimantan Tengah. Masyarakat setempat biasanya menampilkan jenis tarian ini untuk menyambut tamu.</Text> ,no:2},
                ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
               
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;