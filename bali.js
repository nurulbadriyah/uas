import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/bali.jpg')}/>,teks:<Text>1. Tari Kecak</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Selanjutnya ada Tari Kecak dari Bali yang juga masuk ke dalam 34 tari tradisional asal Indonesia.
                Jenis tarian satu ini menggambarkan tentang cerita Pewayangan, khususnya untuk cerita Ramayana yang dipertunjukan dengan seni gerak dan tarian.
                Tari Kecak biasanya dimainkan oleh sekelompok penari laki-laki yang duduk berbaris melingkar yang diiringi dengan irama menyerukan “cak” ketika mengangkat kedua lengan untuk menggambarkan kisah Ramayana ketika barisan kera membantu Rama melawan Rahwana.
                Tarian satu ini sangat terkenal di Bali sekaligus menjadi daya tarik para wisatawan yang tengah berkunjung ke Pulau Dewata tersebut.</Text> ,no:2},
                {satu: <Image source={require('../asset/bali1.jpg')}/>,teks:<Text>2. Tari Pendet</Text> ,no:3},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Salah satu dari 34 tari tradisional asal Indonesia ini mungkin menjadi salah satu jenis tarian yang cukup populer, melihat bahwa tarian satu ini berasal dari Pulau Dewata yang memang masih sangat kental akan pelestarian budayanya.
                Tari Pendet pada awalnya merupakan sebuah tari pemujaan yang banyak diperagakan di Pura, tempat beribadah umat Hindu di Bali.
                Namun lambat laun, para seniman Bali merubah jenis tarian satu ini menjadi “ucapan selamat datang”.</Text> ,no:4},
            ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
                
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;