import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/jakarta.jpg')}/>,teks:<Text>1. Tari Topeng Betawi</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Ternyata, Jakarta tidak hanya terkenal karena kesenian ondel-ondelnya saja lho. Kota yang dipenuhi dengan gedung tinggi ini juga memiliki berbagai kesenian tradisional yang unik, salah satunya seperti Tari Topeng Betawi.
                Tari Topeng Betawi merupakan salah satu tarian adat masyarakat Betawi yang menggunakan topeng sebagai ciri khasnya.
                Perpaduan antara seni tari, musik, dan nyanyian, para penari akan diiringi dengan suara musik dan nyanyian yang lebih bersifat teatrikal dan komunikatif melalui gerakan.</Text> ,no:2},
            ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
               
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;