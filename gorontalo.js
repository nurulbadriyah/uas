import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/gorontalo.jpg')}/>,teks:<Text>1. Tari Saronde</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Tari Saronde merupakan satu dari 34 tari tradisional asal Indonesia yang ditampilkan pada saat prosesi pernikahan adat masyarakat Gorontalo.
                Di Gorontalo itu sendiri, tarian ini diperagakan di dalam serangkaian upacara perkawinan adat masyarakat setempat, di malam pertunangan.
                Nah, biasanya tarian ini ditampilkan oleh para penari pria dan penari wanita yang melakukan gerakan khas sembari menggunakan selendang sebagai atributnya.
                Jenis tarian ini merupakan salah satu tari tradisional yang cukup terkenal di masyarakat Gorontalo itu sendiri.</Text> ,no:2},
                ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
                
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;