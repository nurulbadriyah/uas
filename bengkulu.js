import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/bengkulu.jpg')}/>,teks:<Text>1. Tari Andun</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Daftar 34 Tari tradisional asal Indonesia selanjutnya adalah Tari Andun yang berasal dari Bengkulu.
                Di Bengkulu itu sendiri, Tari Andun biasanya dilakukan pada saat pesta perkawinan, yang mana para Bujang dan gadis menari secara berpasangan pada malam hari sembari diiringi oleh musik kolintang.
                Pada zaman dahulu, jenis tarian satu ini digunakan sebagai sarana mencari jodoh setelah selesai panen padi.
                Namun saat ini sebagai bentuk pelestarian dari salah satu tari tradisional asal Indonesia, maka Tari Andun masih sering kali digunakan sebagai salah satu sarana hiburan bagi masyarakat.</Text> ,no:2},
            ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
                
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;