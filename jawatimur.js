import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/jawatimur.jpg')}/>,teks:<Text>1. Tari Reog Ponorogo</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Tarian Reog Ponorogo merupakan tarian daerah yang berasal dari Jawa Timur bagian Barat-laut dan Ponorogo dianggap sebagai kota asal Reog itu sebenarnya.
                Maka dari itu, di gerbang kota Ponorogo kamu akan bisa melihat hiasan sosok warok dan gemblak yang merupakan dua sosok yang ikut tampil pada saat tari Reog dipertunjukan.
                Reog merupakan salah satu dari 34 tari tradisional asal Indonesia yang masih sangat kental dengan hal-hal yang berbau mistis serta ilmu kebatinan yang kuat.
                Terdapat lima versi cerita yang berkembang, namun yang paling terkenal adalah cerita tentang pemberontakan Ki Ageng Kutu.
                Diceritakan bahwa Ki Ageng Kutu merupakan seorang abdi kerajaan pada masa Bhre Kertabhumi pada abad ke-15.
                Ia melakukan pemberontakan karena murka akan pemerintahan raja yang korup dan terpengaruh kuat oleh istri raja Majapahit yang berasal dari Cina.
                Kemudian ia pun meninggal kan sang raja dan mendirikan perguruan bela diri. Namun, ia pun sadar bahwa pasukannya masih terlalu kecil apabila melawan pasukan kerajaan.
                Maka dari itu, ia pun membuat pertunjukan seni Reog yang menjadi sindiran untuk raja Kertabhumi dan kerajaannya.</Text> ,no:2},
                {satu: <Image source={require('../asset/jawatimur1.jpg')}/>,teks:<Text>1. Tari Kuda Lumping</Text> ,no:3},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Selain Tari Merak, Tari Kuda Lumping juga merupakan salah satu dari 34 tari tradisional asal Indonesia yang cukup populer.
                Berasal dari Ponorogo, Jawa Timur, tarian ini menampilkan sekelompok prajurit yang tengah menunggang kuda yang terbuat dari bambu atau bahan lainnya yang dianyam dan dipotong hingga menyerupai kuda.
                Biasanya, jenis tarian ini hanya menampilkan adegan prajurit berkuda saja. Tetapi tidak jarang pula terkadang salah satu dari 34 tari tradisional asal Indonesia ini juga turut menyuguhkan atraksi kesurupan, kekebalan, serta kekuatan magis seperti memakan beling ataupun kekebalan tubuh terhadap pecutan.</Text> ,no:4},
            ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
               
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;