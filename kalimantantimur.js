import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/kalimantantimur.jpg')}/>,teks:<Text>1. Tari Papatai</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                34 tari tradisional selanjutnya adalah Tari Papatai yang merupakan tarian perang tradisional dari masyarakat suku Dayak di Kalimantan Timur.
                Menggambarkan keberanian lelaki ketika menghadapi perang, tarian ini didominasi oleh gerakan yang gesit, lincah, serta akrobatik yang dipadukan dengan seni teatrikal dan seni tari.
                Di dalam peragaannya, Tari Papatai dibawakan oleh dua penari pria yang dibalut dengan busana adat Dayak yang disebut sebagai Sapei Sapaq dan biasanya dipentaskan dalam acara menyambut tamu kehormatan atau pada kegiatan budaya lainnya.</Text> ,no:2},
                {satu: <Image source={require('../asset/kalimantantimur1.jpg')}/>,teks:<Text>1. Tari Jepen</Text> ,no:3},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Tari Jepen merupakan salah satu dari 34 tari tradisional asal Indonesia yang berasal dari Kalimantan Timur dan dikembangkan oleh suku Kutai dan suku Banjar, namun terdapat sedikit pengaruh dari kebudayaan Melayu dan Islam di dalam peragaannya.
                Dahulu kala, jenis tarian satu ini berfungsi sebagai hiburan dalam rangka penobatan raja-raja dari Kesultanan Kutai Kartanegara di Tenggarong dan sebagai tari pergaulan untuk anak-anak muda setempat.
                Namun sejak tahun 1970-an, tarian ini juga turut digunakan di dalam acara penyambutan tamu-tamu daerah, upacara perkawinan, serta mengisi acara-acara besar lainnya.</Text> ,no:4},
                ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
               
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;