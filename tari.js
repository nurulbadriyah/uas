import React, { Component } from 'react';
import { StyleSheet,Text,View,TouchableOpacity, FlatList, Dimensions} from 'react-native';

const numColumns = 1;
const WIDTH = Dimensions.get('window').width;

class WelcomeAuth extends Component {
    constructor (props){
        super(props);
        this.state = {
            dewi : [
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('aceh')}><Text style={{color:'white'}}>ACEH</Text></TouchableOpacity>,no:1},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('bali')}><Text style={{color:'white'}}>BALI</Text></TouchableOpacity>,no:2},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('jambi')}><Text style={{color:'white'}}>JAMBI</Text></TouchableOpacity>,no:3},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('jawa barat')}><Text style={{color:'white'}}>JAWA BARAT</Text></TouchableOpacity>,no:4},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('jawa timur')}><Text style={{color:'white'}}>JAWA TIMUR</Text></TouchableOpacity>,no:5},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('jawa tengah')}><Text style={{color:'white'}}>JAWA TENGAH</Text></TouchableOpacity>,no:6},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('jakarta')}><Text style={{color:'white'}}>JAKARTA</Text></TouchableOpacity>,no:7},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('sulawesi selatan')}><Text style={{color:'white'}}>SULAWESI SELATAN</Text></TouchableOpacity>,no:8},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('bengkulu')}><Text style={{color:'white'}}>BENGKULU</Text></TouchableOpacity>,no:9},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('banten')}><Text style={{color:'white'}}>BANTEN</Text></TouchableOpacity>,no:10},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('sumatera barat')}><Text style={{color:'white'}}>SUMATERA BARAT</Text></TouchableOpacity>,no:11},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('sumatera utara')}><Text style={{color:'white'}}>SUMATERA UTARA</Text></TouchableOpacity>,no:12},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('gorontalo')}><Text style={{color:'white'}}>GORONTALO</Text></TouchableOpacity>,no:13},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('kalimantan tengah')}><Text style={{color:'white'}}>KALIMANTAN TENGAH</Text></TouchableOpacity>,no:14},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('kalimantan timur')}><Text style={{color:'white'}}>KALIMANTAN TIMUR</Text></TouchableOpacity>,no:15},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('papua')}><Text style={{color:'white'}}>PAPUA</Text></TouchableOpacity>,no:16},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('lampung')}><Text style={{color:'white'}}>LAMPUNG</Text></TouchableOpacity>,no:17},
            ]
        }
    }
    render (){
    return(
            <View style={{flex:1, alignItems:'center',}}>
            <FlatList
                data={this.state.dewi}
                renderItem = {({item,index}) => (
                    <View style={{alignItems:'center',marginTop:10,backgroundColor:'maroon',borderRadius:15}}>
                        {item.satu}
                    </View>
                )}
                keyExtractor = {(item) => item.no}
                numColumns = {numColumns}
            />
            </View>
    )
    }
}
export default WelcomeAuth;

const styles = StyleSheet.create({
    sans : {
        height : numColumns / (WIDTH*2),
        padding:5,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'green',
        fontSize: 50,
        color:'white'

    }

});
