import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/sumaterautara.jpg')}/>,teks:<Text>1. Tari Piso Surit</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Tari Piso Surit berasal dari suku Batak Karo, Sumatera Utara yang biasanya digunakan untuk menyambut para tamu kehormatan di suatu acara adat setempat.
                Piso Surit itu sendiri memiliki arti burung yang bernyanyi yang mana juga menggambarkan seorang gadis yang sedang menantikan kedatangan dari sang kekasih.
                Biasanya, jenis tarian ini ditampilkan oleh sekelompok wanita dan pria yang terdiri dari lima pasang atau lebih. Kemudian penari menggunakan busana adat dan menari sembari diiringi oleh musik tradisional.</Text> ,no:2},
                ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
               
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;