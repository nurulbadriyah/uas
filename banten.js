import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/banten.jpg')}/>,teks:<Text>1. Tari Bendrong Lesung</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Tari Bendrong Lesung merupakan salah dari 34 tari tradisional asal Indonesia yang berasal dari Provinsi Banten.
                Disebut sebagai Bendrong Lesung karena jenis tarian ini menggunakan sarana berupa lesung dan alu yang merupakan sebuah alat dalam menumbuk padi atau beras.
                Tarian ini biasanya dilakukan oleh sekelompok wanita dewasa sebagai ritual masyarakat Banten, terutama ketika tibanya masa panen raya.
                Namun seiring berjalannya waktu, Tari Bendrong Lesung juga mulai dilakukan oleh kaum remaja hingga anak-anak.</Text> ,no:2},
            ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
                
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;