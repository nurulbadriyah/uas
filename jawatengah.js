import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/jawatengah.jpg')}/>,teks:<Text>1. Tari Serimpi</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Tari Serimpi merupakan salah satu dari 34 tari tradisional asal Indonesia yang berasal dari Jawa Tengah (Surakarta) dan Yogyakarta.
                Tarian ini dapat dicirikan dengan empat penari melakukan gerakan gemulai yang menggambarkan kesopanan, kehalusan budi, serta kelemah lembutan yang ditunjukan dari gerakan yang pelan nan anggun sembari diiringi oleh suara musik gamelan.
                Sejak dari zaman kuno, Tari Serimpi telah memiliki kedudukan yang cukup istimewa, khususnya di keraton-keraton Jawa yang tidak dapat disandingkan dengan pertunjukan tari lainnya, karena sifat tarian ini yang sakral. Dahulu kala, jenis tarian ini hanya boleh diperagakan oleh orang-orang pilihan keraton.</Text> ,no:2},
                ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
                
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;