import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/jambi.jpg')}/>,teks:<Text>1. Tari Sekapur Sirih</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Tari Sekapur Sirih merupakan tari tradisional asal Indonesia yang berasal dari Provinsi Jambi, Kepulauan Riau dan Riau.
                Tarian ini biasanya menjadi tarian selamat datang guna menyambut para tamu-tamu besar yang hadir di sebuah acara.
                Menggambarkan ungkapan rasa putih hati masyarakat dalam menyambut tamu, salah satu tarian dari 34 tari tradisional asal Indonesia ini biasanya ditarikan oleh 9 orang penari perempuan, 3 orang penari laki-laki, 1 orang yang bertugas membawa payung, serta 2 orang pengawal.</Text> ,no:2},
                {satu: <Image source={require('../asset/jambi1.jpg')}/>,teks:<Text>2. Tari Selampit Delapan</Text> ,no:3},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                34 Tari tradisional asal Indonesia selanjutnya adalah Tari Selampit Delapan yang berasal dari Provinsi Jambi dan diperkenalkan untuk pertama kalinya oleh M. Ceylon yang merupakan seorang fotografer senior kelahiran Padang Sidempuan yang pada saat itu tengah bertugas di Dinas Kebudayaan Provinsi Jambi pada tahun 1970-an.
                Diberi nama “Selampit Delapan” karena tarian ini menggunakan 8 tali di dalam gerakan tariannya.
                Namun saat ini, tali yang pada awalnya digunakan sebagai atribut mulai diganti menjadi selendang agar tarian terlihat lebih menarik.</Text> ,no:4},
            ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
                
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;