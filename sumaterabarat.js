import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/sumaterabarat.jpg')}/>,teks:<Text>1. Tari Piring</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Tari Piring atau tari piring dalam bahasa Minangkabau merupakan sebuah tarian dari 34 tari tradisional asal Indonesia yang melibatkan atraksi piring di dalamnya.
                Nah, para penari ini mengayunkan piring sembari mengikuti gerakan-gerakan cepat yang teratur namun tetap stabil dalam menggenggam piring di tangannya tanpa terlepas.
                Gerakan-gerakan yang digunakan di dalam tari piring sebagian besar diambil dari langkah-langkah dalam silat Minangkabau atau silek.
                Secara tradisional, jenis tarian ini berasal dari Solok, Sumatera Barat. Tetapi secara umum, tarian ini juga merupakan simbol dari masyarakat Minangkabau.</Text> ,no:2},
                {satu: <Image source={require('../asset/sumaterabarat1.jpg')}/>,teks:<Text>2. Tari Lilin</Text> ,no:3},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Selanjutnya ada Tari lilin yang juga masuk ke dalam 34 tari tradisional asal Indonesia. Pada dasarnya, jenis tarian satu ini merupakan sebuah tarian yang diperagakan oleh sekelompok penari yang membawa lilin menyala di atas sebuah piring di telapak tangan mereka.
                Berasal dari Sumatera Barat, Tari Lilin pada awalnya hanya ditampilkan di dalam acara adat, sebagai ungkapan rasa syukur kepada Tuhan atas pencapaian yang telah didapat oleh masyarakat setempat.
                Namun seiring dengan berjalannya waktu, tarian ini tidak hanya digunakan sebagai ucapa rasa syukur, melainkan juga sekaligus sebagai kesenian dan hiburan.
</Text> ,no:4},
            ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
               
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;