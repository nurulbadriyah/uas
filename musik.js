import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/gong.jpg')}/>,teks:<Text>1. Alat Musik Tradisional Gong</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Kamu tentu sudah tidak asing dengan alat musik tradisional gong. Gong merupakan salah satu alat musik tradisional Jawa Tengah. Gong termasuk ke dalam alat musik idiofon, yang maksudnya menghasilkan suara dari getaran keseluruhan alat musik itu sendiri.</Text> ,no:2},
                {satu: <Image source={require('../asset/rebab.jpg')}/>,teks:<Text>2. Alat Musik Tradisional Rebab</Text> ,no:3},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Alat musik tradisional yang berbentuk seperti busur panah ini bernama rebab. Rebab merupakan alat musik tradisional dari Jawa Barat. Kamu bisa memainkan alat musik rebab dengan digesek seperti biola.</Text> ,no:4},
                {satu: <Image source={require('../asset/kecapi.jpg')}/>,teks:<Text>3. Alat Musik Tradisional Kecapi</Text> ,no:5},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Kecapi termasuk sebagai alat musik kordofon. Kordofon merupakan alat musik yang mengeluarkan suara dari sebuah senar atau dawai.Kecapi sendiri merupakan alat musik tradisional yang berasal dari Sunda yang dimainkan sebagai alat musik utama dalam Tembang Sunda atau Mamaos Cianjuran dan Kacapi suling. Cara memainkan alat musik kecapi adalah dengan dipetik bagian senarnya.</Text> ,no:6},
                {satu: <Image source={require('../asset/kolintang.jpg')}/>,teks:<Text>4. Alat Musik Tradisional Kolintang</Text> ,no:7},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Kolintang adalah alat musik tradisional dari Minahasa, Sulawesi Utara, yang berbentuk segiempat trapesium dengan bilah-bilah kayu berukuran berbeda. Setiap bilah kayu akan menghasilkan suara berbeda pula jika dipukul. Karena itu, kolintang termasuk alat musik idiofon karena mengeluarkan suara dari getarannya sendiri.Rakyat Minahasa beranggapan bahwa nama kolintang berasal dari suaranya, yaitu tong (suara rendah), ting (suara tinggi), dan tang (suara umum). Tong ting tang beralih jadi kata kulintang agar mudah dilafalkan oleh penduduk Minahasa.</Text> ,no:8},
                {satu: <Image source={require('../asset/bonang.jpg')}/>,teks:<Text>5. Alat Musik Tradisional Bonang</Text> ,no:9},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Bonang adalah alat musik tradisional dari Jawa Timur. Bonang sendiri terbuat dari bahan kuningan. Dan biasanya bonang digunakan untuk memperingati upacara adat setempat.Bonang juga merupakan salah satu alat musik daerah asli Indonesia yang digunakan dalam pagelaran gamelan.</Text> ,no:10},
                {satu: <Image source={require('../asset/gendang.jpg')}/>,teks:<Text>6. Alat Musik Tradisional Gendang</Text> ,no:11},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Sebetulnya di setiap daerah memiliki alat musik tradisional yang bentuknya menyerupai gendang. Dan biasanya memiliki nama-nama yang berbeda di setiap daerahnya. Namun gendang sendiri berasal dari Daerah Istimewa Yogyakarta.Gendang juga merupakan salah satu alat musik yang digunakan pada pagelaran musik gamelan. Alat musik gendang dimainkan dengan cara dipukul atau diketuk pada bagian kulit yang ada di sisi kanan dan kiri alat musik.</Text> ,no:12},
                {satu: <Image source={require('../asset/anklung.jpg')}/>,teks:<Text>7. Alat Musik Tradisional Angklung</Text> ,no:13},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Angklung merupakan alat musik tradisional yang masih cukup sering digunakan. Cukup banyak musisi yang menggunakan angklung layaknya sebuah orkestra.Angklung terbuat dari bambu, dan kamu bisa memainkan alat musik tradisional ini dengan cara menggoyangkannya sampai bambu tersebut mengeluarkan suara akibat getaran.</Text> ,no:14},
                {satu: <Image source={require('../asset/sampe.jpg')}/>,teks:<Text>8. Alat Musik Tradisional Sampe</Text> ,no:15},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Mungkin nama sampe masih cukup asing bagi orang kebanyakan. Ya, karena sampe sendiri berasal dari daerah Kalimantan Timur.Sampe termasuk sebagai alat musik kordofon, yang mengeluarkan bebunyian dari dawai atau senar. Jika dilihat dari bentuknya, sampe cukup mirip dengan alat musik kecapi.</Text> ,no:16},
                {satu: <Image source={require('../asset/sampe.jpg')}/>,teks:<Text>9. Alat Musik Tradisional Aramba</Text> ,no:17},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Jika dilihat dengan sekilas, aramba memiliki bentuk yang sama seperti gong. Namun aramba bertubuh lebih kecil.Cara memainkan kedua alat musik ini pun sama. Aramba juga dimainkan dengan cara dipukul menggunakan semacam stik untuk menimbulkan suara.</Text> ,no:18},
                {satu: <Image source={require('../asset/doli.jpg')}/>,teks:<Text>10. Alat Musik Tradisional Doli-doli</Text> ,no:19},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Alat musik tradisional daerah Bengkulu ini memiliki bentuk yang cukup mirip dengan gendang. Namun jika gendang memiliki dua sisi yang bisa dipukul, doli-doli hanya memiliki satu di bagian atas. Sedangkan bagian bawah alat musik ini berbentuk bulat. Jadi jika dilihat secara keseluruhan alat musik ini memiliki bentuk setengah lingkaran.</Text> ,no:20},
                {satu: <Image source={require('../asset/ganda.jpg')}/>,teks:<Text>11. Alat Musik Tradisional Ganda</Text> ,no:21},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Ganda atau Kanda juga merupakan alat musik tradisional Gorontalo yang serupa dengan gendang. Alat musik tradisional dari Sulawesi ini juga dimainkan dengan cara dipukul pada bagian kulitnya saja.Namun Ganda memiliki tubuh yang lebih ramping dan tinggi. Tidak seperti gendang atau doli-doli yang bertubuh lebih pendek dan tebal.</Text> ,no:22},
                {satu: <Image source={require('../asset/talindo.jpg')}/>,teks:<Text>12. Alat Musik Tradisional Talindo</Text> ,no:23},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Di daerah Bugis, alat musik tradisional ini memiliki sebutan Talindo. Namun berbeda dengan di Makassar. Di daerah Makassar, alat musik ini disebut dengan nama Popondi.Biasanya alat musik tradisional ini dimainkan sebagai perayaan sesudah para petani merayakan pesta panen.Talindo adalah alat musik petik yang hanya memiliki satu buah senar untuk dimainkan. Bentuknya yang unik menjadi daya tarik tersendiri untuk alat musik ini.</Text> ,no:24},
                {satu: <Image source={require('../asset/geso.jpg')}/>,teks:<Text>13. Alat Musik Tradisional Geso-geso</Text> ,no:25},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Geso-geso juga memiliki bentuk menyerupai biola dan kecapi. Alat musik tradisional Indonesia dari suku Toraja ini terbuat dari kayu dan tempurung kelapa yang diberi dawai atau senar.Untuk memainkannya, kamu hanya perlu menggesek alat musik tradisional ini dengan sebuah alat khusus yang terbuat dari bilah bambu dan tali.</Text> ,no:26},
                {satu: <Image source={require('../asset/lalove.jpg')}/>,teks:<Text>14. Alat Musik Tradisional Lalove</Text> ,no:27},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Lalove adalah alat musik tradisional Indonesia yang berasal dari daerah Sulawesi Tengah. Sama-sama terbuat dari bambu, membuat alat musik ini kerap kali disama-samakan dengan suling.Sebetulnya lalove adalah alat musik yang sakral, karena tidak semua orang boleh memainkan alat musik ini. Dan biasanya lalove digunakan untuk mengiri tarian tradisional Balia yang merupakan ritual penyembuhan suku Kaili di Sulawesi Tengah.</Text> ,no:28},
                {satu: <Image source={require('../asset/tanduak.jpg')}/>,teks:<Text>15. Alat Musik Tradisional Pupuik Tanduak</Text> ,no:29},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Masyarakat Minangkabau membuat alat musik daerah ini dari tanduk hewan kerbau. Alat musik tradisional ini dibuat dengan cara memotong ujung tanduk sehingga membentuk rongga sampai pada pangkalnya. Saat meniup pupuik tanduak, alat musik ini akan mengeluarkan suara yang menyerupai terompet namun dengan nada yang lebih tinggi dan melengking.</Text> ,no:30},
            ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
                
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;