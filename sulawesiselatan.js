import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/sulawesiselatan.jpg')}/>,teks:<Text>1. Tari Kipas Pakarena</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Tari Kipas Pakarena merupakan salah satu ekspresi kesenian masyarakat Gowa yang sering dipentaskan untuk mempromosikan pariwisata Sulawesi Selatan.
                Dalam bahasa setempat, “pakarena” berasal dari kata “karena” yang memiliki arti “main”. Maka dari itu, apabila diartikan maka Tari Kipas Pakarena juga memiliki arti “tarian bermain kipas”.
                Jenis tarian satu ini memiliki peraturan yang cukup unik, dimana penari tidak diperbolehkan untuk membuka matanya terlalu lebar, sedangkan gerakan kakinya juga tidak boleh diangkat terlalu tinggi.
                Tarian ini biasanya berlangsung selama dua jam, sehingga para penarinya juga diharuskan untuk memiliki kondisi fisik yang prima.</Text> ,no:2},
            ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
               
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;