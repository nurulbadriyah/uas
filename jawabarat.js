import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/jawabarat.jpg')}/>,teks:<Text>1. Tari Jaipong</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Tari Jaipong merupakan salah satu jenis tari asal Bandung, Jawa Barat dan sudah cukup populer, khususnya di telinga masyarakat Indonesia.
                Diciptakan oleh Seniman berdarah Sunda, Gugum Gumbira dan Haji Suanda, tarian ini merupakan gabungan dari beberapa kesenian seperti Wayang Golek, Pencak Silat, dan Ketuk Tilu.
                Maka dari itu, tak heran apabila gerakannya cenderung enerjik namun juga unik, dengan disertai oleh iringan alat musik degung.
                Bahkan, tak jarang tarian ini sering mengundang gelak tawa para penonton karena keunikannya yang dihasilkan.</Text> ,no:2},
                {satu: <Image source={require('../asset/jawabarat1.jpg')}/>,teks:<Text>1. Tari Merak</Text> ,no:3},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Tari Merak merupakan salah satu dari 34 tari tradisional asal Indonesia yang cukup terkenal dna merupakan jenis tarian khas yang berasal dari Provinsi Jawa Barat.
                Sesuai dengan namanya, tarian ini merupakan bentuk ekspresi dari kehidupan seekor burung Merak.
                Gerakan-gerakannya pun diambil dari tingkah laku dari burung Merak itu sendiri. Tidak hanya gerakannya yang unik, Tari Merak juga merupakan salah satu jenis tarian modern kontemporer yang mana gerakannya itu sendiri diciptakan secara bebas dengan menggunakan kreasi baru.</Text> ,no:4},
            ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
               
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;