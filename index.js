import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Splash from '../splash/index';
import Home from '../component/home';
import Tari from '../component/tari';
import Musik from '../component/musik';
import Teater from '../component/teater';
import Aceh from '../daerah/aceh';
import Bali from '../daerah/bali';
import Banten from '../daerah/banten';
import Bengkulu from '../daerah/bengkulu';
import Gorontalo from '../daerah/gorontalo';
import Jakarta from '../daerah/jakarta';
import Jambi from '../daerah/jambi';
import Jawabarat from '../daerah/jawabarat';
import Jawatengah from '../daerah/jawatengah';
import Jawatimur from '../daerah/jawatimur';
import Kalimantantengah from '../daerah/kalimantantengah';
import Kalimantantimur from '../daerah/kalimantantimur';
import Lampung from '../daerah/lampung';
import Papua from '../daerah/papua';
import Sulawesiselatan from '../daerah/sulawesiselatan';
import Sumaterabarat from '../daerah/sumaterabarat';
import Sumaterautara from '../daerah/sumaterautara';

const Stack = createStackNavigator();

const Route = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen
                name = "splash"
                component = {Splash}
                options = {{headerShown: false}}
            />
            <Stack.Screen
                name = "home"
                component = {Home}
                options = {{headerShown: false}}
            />
            <Stack.Screen
                name = "tari"
                component = {Tari}
            />
            <Stack.Screen
                name = "musik"
                component = {Musik}
            />
            <Stack.Screen
                name = "teater"
                component = {Teater}
            />
            <Stack.Screen
                name = "aceh"
                component = {Aceh}
            />
            <Stack.Screen
                name = "bali"
                component = {Bali}
            />
            <Stack.Screen
                name = "banten"
                component = {Banten}
            />
            <Stack.Screen
                name = "bengkulu"
                component = {Bengkulu}
            />
            <Stack.Screen
                name = "gorontalo"
                component = {Gorontalo}
            />
            <Stack.Screen
                name = "jakarta"
                component = {Jakarta}
            />
            <Stack.Screen
                name = "jambi"
                component = {Jambi}
            />
            <Stack.Screen
                name = "jawabarat"
                component = {Jawabarat}
            />
            <Stack.Screen
                name = "jawatengah"
                component = {Jawatengah}
            />
            <Stack.Screen
                name = "jawatimur"
                component = {Jawatimur}
            />
            <Stack.Screen
                name = "kalimantantengah"
                component = {Kalimantantengah}
            />
            <Stack.Screen
                name = "kalimantantimur"
                component = {Kalimantantimur}
            />
            <Stack.Screen
                name = "lampung"
                component = {Lampung}
            />
            <Stack.Screen
                name = "papua"
                component = {Papua}
            />
            <Stack.Screen
                name = "sulawesiselatan"
                component = {Sulawesiselatan}
            />
            <Stack.Screen
                name = "sumaterabarat"
                component = {Sumaterabarat}
            />
            <Stack.Screen
                name = "sumaterautara"
                component = {Sumaterautara}
            />
            
        </Stack.Navigator>
    )
}

export default Route;