import React, { Component } from 'react';
import { StyleSheet,Text,View,TouchableOpacity, FlatList, Dimensions} from 'react-native';

const numColumns = 1;
const WIDTH = Dimensions.get('window').width;

class WelcomeAuth extends Component {
    constructor (props){
        super(props);
        this.state = {
            dewi : [
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('tari')}><Text style={{color:'white'}}>SENI TARI</Text></TouchableOpacity>,no:1},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('musik')}><Text style={{color:'white'}}>SENI ALAT MUSIK</Text></TouchableOpacity>,no:2},
                {satu : <TouchableOpacity style={{padding:50}} onPress={() => this.props.navigation.navigate('teater')}><Text style={{color:'white'}}>SENI TEATER</Text></TouchableOpacity>,no:3},
            ]
        }
    }
    render (){
    return(
            <View style={{flex:1, alignItems:'center',}}>
            <FlatList
                data={this.state.dewi}
                renderItem = {({item,index}) => (
                    <View style={{alignItems:'center',marginTop:100,backgroundColor:'maroon',borderRadius:15}}>
                        {item.satu}
                    </View>
                )}
                keyExtractor = {(item) => item.no}
                numColumns = {numColumns}
            />
            </View>
    )
    }
}
export default WelcomeAuth;

const styles = StyleSheet.create({
    sans : {
        height : numColumns / (WIDTH*2),
        padding:5,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'green',
        fontSize: 50,
        color:'white'

    }

});
