import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../asset/aceh.jpg')}/>,teks:<Text>1. Tari Saman Meuseukat</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Tari Saman merupakan salah satu tari tradisional asal Indonesia yang bahkan sudah cukup dikenal di seluruh mancanegara.
                Tarian satu ini merupakan sebuah tarian suku Gayo yang biasa ditampilkan ketika perayaan peristiwa-peristiwa penting di dalam adat.
                Maka dari itu, syair yang terdapat di dalam salah satu tarian dari 34 tari tradisional asal Indonesia ini menggunakan Bahasa Gayo.
                Di dalam beberapa literatur menyebutkan bahwa Tari Saman dikembangkan oleh Syekh Saman yang merupakan seorang ulama asal Gayo di Aceh Tenggara.
                Pada 24 November 2011, Tari Saman ditetapkan UNESCO sebagai Daftar Representatif Budaya Takbenda Warisan Manusia dalam sidang ke-6 Komite Antar-Pemerintah untuk Perlindungan Warisan Budaya Tak Benda UNESCO di Bali.</Text> ,no:2},
            ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
                
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;