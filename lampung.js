import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class anekaragam extends Component{
    constructor(props){
        super(props);
        this.state= {
            nubad : [
                {satu: <Image source={require('../asset/lampung.jpg')}/>,teks:<Text>1. Tari Melinting</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                34 tari tradisional selanjutnya adalah Tari Papatai yang merupakan tarian perang tradisional dari masyarakat suku Dayak di Kalimantan Timur.
                Tari Melinting merupakan salah satu jenis tari tradisional asal Lampung yang menggambarkan keperkasaan serta keagungan Keratuan Melinting.
                Pada awalnya, tarian ini diperagakan sebagai pelengkap dari acara Gawi Adat, yaitu acara Keagungan Keratuan Melinting.
                Karena merupakan tarian untuk keluarga ratu, penarinya pun terbatas dan hanya orang-orang tertentu saja yang boleh melakukannya, seperti putra dan putri Keratuan Melinting.
                Namun, seiring berkembangnya waktu tarian ini mulai berkembang menjadi tarian rakyat yang ditampilkan pada acara-acara budaya sekaligus sebagai tarian dalam menyambut Tamu Agung.</Text> ,no:2},
                ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'white'}}>
               
                <FlatList
                    data ={this.state.nubad}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default anekaragam;